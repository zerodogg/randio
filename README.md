# Randio music player

Randio is a music player for Unix-like operating systems (like GNU/Linux).
It scans your music collection, and then plays you randomly selected songs
from it. You can ban tracks (ensure that you never hear it again), and
love tracks (and then later play a random selection of only tracks you
have previously loved). It can be thought of as a sort of offline/local
version of the old last.fm radio player.

Randio is written in C and uses the Gtk3 and gstreamer libraries.
It supports submitting your played tracks to last.fm.

Randio is **alpha** quality software. It works, but is missing a
lot of functionality.
